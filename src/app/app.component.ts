import { Component } from '@angular/core';
import { FormControl } from '@angular/forms';
import { HttpClient } from '@angular/common/http';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'cotizador';
  origenId: number = 0;
  destinationId: number = 1;
  origenJson: Origen[] = [{ id: 0, name: 'Guatemala' }];
  destinationJson: Destination[] = [];
  peso = new FormControl('');
  ancho = new FormControl('');
  alto = new FormControl('');
  largo = new FormControl('');
  cliente = new FormControl('');
  Tarifa = new FormControl('');
  existeCliente: Boolean = false;
  name: string;
  tarifaCliente: string;
  resTarifa: number;
  regionJson: any[] = [];
  regionId: any = 1;
  TipoTarifa: string;
  constructor(private http: HttpClient) { }
  ngOnInit(): void {
    this.getRegion();
  }
  getcliente() {
    this.existeCliente = false;
    var codigo = this.cliente.value;
    this.http.get("http://localhost:21658/api/cotizador/Cliente/" + codigo).subscribe(
      result => {
        if (result !== null) {
          this.existeCliente = true;
          this.name = result[0].nombre + ' ' + result[0].apellido;
        } else {
          this.existeCliente = false;
        }
      },
      error => {
        console.log('problemas');
      }
    );
  }
  getTarifa() {
    var codigo = this.cliente.value;
    this.http.get("http://localhost:21658/api/cotizador/Servicio/Cliente/" + codigo + "/Pais/" + this.destinationId).subscribe(
      (result: any) => {
        if (result.length > 0) {
          this.tarifaCliente = result[0].tarifa;
          //calculo de tarifa         
          this.resTarifa = Math.round((Number(this.peso.value) * Number(this.tarifaCliente)) + 1.66 * (Number(this.alto.value) * Number(this.largo.value) * Number(this.ancho.value)) - 1.50 * (0.5 * Number(this.peso.value)));
          this.TipoTarifa = "Premium";
        } else {
          //si es cliente pero no tiene las tarifas de ese pais se le da siempre la Premium
          if (this.existeCliente) {
            this.getTarifaDefault();
          } else {
            //si no es cliente le cobro a 5.00 la tarifa                   
            this.resTarifa = Math.round((Number(this.peso.value) * 5.00) + 1.66 * (Number(this.alto.value) * Number(this.largo.value) * Number(this.ancho.value)) - 1.50 * (0.5 * Number(this.peso.value)));
            this.TipoTarifa = "Estandar";
          }
        }
      },
      error => {
        console.log('problemas');
      }
    );
  }

  getTarifaDefault() {
    this.http.get("http://localhost:21658/api/cotizador/servicio").subscribe((res: any) => {
      if(res !== null){
        this.tarifaCliente = res.tarifa;
          //calculo de tarifa         
          this.resTarifa = Math.round((Number(this.peso.value) * Number(this.tarifaCliente)) + 1.66 * (Number(this.alto.value) * Number(this.largo.value) * Number(this.ancho.value)) - 1.50 * (0.5 * Number(this.peso.value)));
          this.TipoTarifa = "Default";
      }else{
        console.log('Not found getTarifaDefault');
      }
    },
      error => {
        console.log('Not found');
      })
  }

  getPais() {
    var idRegion = this.regionId;
    this.http.get("http://localhost:21658/api/cotizador/pais/" + idRegion).subscribe(
      (result: any) => {
        console.log(result);
        if (result !== null) {
          this.destinationJson = result;
          this.destinationId = this.destinationJson[0].id;
        } else {

        }
      },
      error => {
        console.log('Not found');
      }
    );
  }

  getRegion() {
    this.http.get("http://localhost:21658/api/cotizador/region").subscribe((res: any) => {
      if (res.length > 0) {
        this.regionJson = res;
        this.getPais();
      } else {
        alert("No se encontraron regiones");
      }
    },
      error => {
        console.log('Not found');
      }
    );
  }
}

interface Origen {
  id: number;
  name: string;
}
interface Destination {
  id: number;
  name: string;
}
